/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {
    static char[][] board;
    static char turn = 'X';
    static int row;
    static int col;
        
    public static void main(String[] args) {
        printwelcome();
        makeboard();
        while(true){
            printboard();
            printturn();
            inputrow_col_and_check();
            if(wingame()){
                printboard();
                printwin();
                break;
            }
            if(draw()){
                printboard();
                printdraw();
                break;
            }
            changeturn();
        }
        
    }
    static void printwelcome(){
        System.out.println("Welcome to tic tac toe");
    }
    static void makeboard(){
        board = new char[3][3];
        for(int i =0;i<3;i++){
            for(int j =0;j<3;j++){
                board[i][j] = '-';
            }
        }
    }
    static void printboard(){
        for(int i =0;i<3;i++){
            for(int j =0;j<3;j++){
                System.out.print(board[i][j]+" ");
            }
            System.out.println(" ");
        }
    }
    static void printturn(){
        System.out.println(turn+" turn");
    }
    static void inputrow_col_and_check(){
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.println("enter a row");
            row = kb.nextInt();
            System.out.println("enter a col");
            col = kb.nextInt();
            row = row-1;
            col = col-1;
            if(board[row][col] != '-'){
                System.out.println("invalid row or col");
            }else if(row <0 || row >2||col <0 || col>2){
                System.out.println("invalid row or col");
            }else{
                board[row][col] = turn;
                break;
            }
        }
    }
    static void changeturn(){
        if(turn == 'X'){
            turn = 'O';
        }else{
            turn = 'X';
        }
    }
    static boolean wingame(){
        if(checkwinrow()){
            return true;
        }
        if(checkwincol()){
            return true;
        }
        if(checkwindiago()){
            return true;
        }
        return false;
    }
    
    static boolean checkwinrow(){
        for(int i=0;i<3;i++){
            if(board[row][i]!=turn){
                return false;
            }
        }
        return true;
    }
    static boolean checkwincol(){
        for(int j=0;j<3;j++){
            if(board[j][col]!=turn){
                return false;
            }
        }
        return true;
    }
    static boolean checkwindiago(){
        if(row == col && board[0][0]==turn && board[1][1] == turn && board[2][2] == turn) {
            return true;
        }else if (row + col == 2 && board[0][2] == turn && board[1][1] == turn && board[2][0] == turn) {
            return true;
        }
        return false;
    }
    
    static void printwin(){
        System.out.println(turn+" has Win!!");
    }
    
    static boolean draw(){
        for(int i =0;i<3;i++){
            for(int j =0;j<3;j++){
                if(board[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
    static void printdraw(){
        System.out.print("it draw!!");
    }
    
}

